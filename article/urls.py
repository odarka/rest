from rest_framework.routers import DefaultRouter
from .views import ArticleViewSet, CarViewSet

router = DefaultRouter()
router.register(r'articles', ArticleViewSet, basename='user')
router.register(r'cars', CarViewSet, basename='user')
urlpatterns = router.urls
