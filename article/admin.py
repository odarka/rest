from django.contrib import admin

from .models import Article, Author, Car

admin.site.register(Article)
admin.site.register(Author)


@admin.register(Car)
class CarAdmin(admin.ModelAdmin):
    """Car admin."""

    list_display = ('name',)
    search_fields = ()
